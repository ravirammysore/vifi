package com.appfocus.vifi.printers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.widget.Toast;

import com.appfocus.vifi.activities.ReceiptActivity;

public class NgxBTHelper {
    public static final String title_connecting = "connecting...";
    public static final String title_connected_to = "connected: ";
    public static final String title_not_connected = "not connected";

    public static void unpairDevice(Context context) {
        AlertDialog.Builder u = new AlertDialog.Builder(context);
        u.setTitle("Bluetooth NgxBTHelper unpair");
        // d.setIcon(R.drawable.ic_launcher);
        u.setMessage("Are you sure you want to unpair all Bluetooth printers ?");
        u.setPositiveButton(android.R.string.yes,
                (dialog, which) -> {
                    if (ReceiptActivity.mBtp.unPairBluetoothPrinters())
                        Toast.makeText(context, "All NGX Bluetooth printer(s) unpaired", Toast.LENGTH_SHORT).show();
                });
        u.setNegativeButton(android.R.string.no,
                (dialog, which) -> {
                    // do nothing
                });
        u.show();
    }

    public static void clearDevice(Context context) {
        // deletes the last used printer, will avoid auto connect
        AlertDialog.Builder d = new AlertDialog.Builder(context);
        d.setTitle("NGX Bluetooth NgxBTHelper");
        // d.setIcon(R.drawable.ic_launcher);
        d.setMessage("Are you sure you want to delete your preferred Bluetooth printer ?");
        d.setPositiveButton(android.R.string.yes,
                (dialog, which) -> {
                    ReceiptActivity.mBtp.clearPreferredPrinter();
                    Toast.makeText(context, "Preferred Bluetooth printer cleared", Toast.LENGTH_SHORT).show();
                });
        d.setNegativeButton(android.R.string.no,
                (dialog, which) -> {
                    // do nothing
                });
        d.show();
    }

    public static void showDeviceList(Activity callingActivity) {
        // show a dialog to select from the list of available printers
        try {
            ReceiptActivity.mBtp.showDeviceList(callingActivity);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
