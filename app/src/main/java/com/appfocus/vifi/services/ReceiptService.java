package com.appfocus.vifi.services;

import android.content.Context;

import com.appfocus.vifi.models.Receipt;
import com.appfocus.vifi.sqlconfig.SQLServer;

import java.sql.Connection;
import java.util.List;


public class ReceiptService extends BaseService {
    Connection mConnection;
    SQLServer mSQLServer;

    public  ReceiptService(Context context){
        mSQLServer = new SQLServer(context);
    }

    public String getNextReceiptCode(){
        return Receipt.getNextReceiptCode(mRealm);
    }

    public Receipt getReceipt(String receiptGUID) {
        Receipt receipt = Receipt.get(receiptGUID,mRealm);
        return receipt!=null? mRealm.copyFromRealm(receipt) : null;
    }

    public List<Receipt> getReceipts(){
        List<Receipt> receipts = Receipt.getAll(mRealm);
        return receipts!=null ? mRealm.copyFromRealm(receipts) :null;
    }

    public int getReceiptsCount(){
        return Receipt.getReceiptsCount(mRealm);
    }

    public boolean clearAllReceipts(){
        return Receipt.clearAll(mRealm);
    }

    public boolean AddOrUpdateReceipt(Receipt receipt){
        return Receipt.AddOrUpdate(receipt,mRealm);
    }

    public boolean  push_update_clear(){
        boolean allDone = false;

        try {
            mConnection = mSQLServer.GetConnection();
            allDone = Receipt.push_update_clear(mConnection,mRealm);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return allDone;
    }
}
