package com.appfocus.vifi.services;

import com.appfocus.vifi.models.LoanBalance;

import java.util.List;

public class LoanBalanceService extends BaseService {

    public List<LoanBalance> getLoanBalances(){
        List<LoanBalance> balances =  LoanBalance.getAll(mRealm);
        return balances!=null ? mRealm.copyFromRealm(balances) : null;
    }

    public LoanBalance getLoanByNo(int loanNo) {
        LoanBalance balance = LoanBalance.get(loanNo,mRealm);
        return balance!=null ? mRealm.copyFromRealm(balance):null;
    }

    public boolean deleteLoanRecordsinMobile(){
        boolean result = LoanBalance.deleteLoanRecordsInMobile(mRealm);
        return result;
    }
}
