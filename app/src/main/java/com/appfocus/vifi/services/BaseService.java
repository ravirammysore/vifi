package com.appfocus.vifi.services;

import io.realm.Realm;

public abstract class BaseService {
    Realm mRealm;

    public BaseService() {
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    protected void finalize() throws Throwable {
        closeRealmConnection();
        super.finalize();
    }

    private void closeRealmConnection() {
        try {
            if(mRealm!=null && !mRealm.isClosed())
                mRealm.close();
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }
}
