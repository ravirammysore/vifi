package com.appfocus.vifi.services;

import android.content.Context;

import com.appfocus.vifi.models.CollectionAgent;
import com.appfocus.vifi.models.ErrorLog;
import com.appfocus.vifi.models.LoanBalance;
import com.appfocus.vifi.models.Receipt;
import com.appfocus.vifi.sqlconfig.SQLServer;

import java.sql.Connection;
import java.util.List;

public class SyncService extends BaseService {

    Connection mConnection;
    SQLServer mSQLServer;

    public SyncService(Context context){
        super();
        mSQLServer = new SQLServer(context);
    }

    public boolean pullCollectionAgentsAndSaveLocally(){
        boolean success = false;
        try {
            mConnection = mSQLServer.GetConnection();
            success = CollectionAgent.pullCollectionAgentsAndSaveLocally(mConnection, mRealm);
        }
        catch (Exception e) {
            e.printStackTrace();
            ErrorLog.LogError(e);
        }
        return success;
    }

    public boolean pullLoanBalancesAndSaveLocally(){
        mConnection = mSQLServer.GetConnection();
        return LoanBalance.pullLoanBalancesAndSaveLocally(mConnection,mRealm);
    }


    @Override
    protected void finalize() throws Throwable {
        closeSqlConnection();
        super.finalize();
    }

    private void closeSqlConnection() {
        try {
            if(mConnection!=null && !mConnection.isClosed())
                mConnection.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            ErrorLog.LogError(e);
        }
    }
}
