package com.appfocus.vifi.services;

import android.content.Context;

import com.appfocus.vifi.models.CollectionAgent;

public class AuthService extends BaseService {

    Context mContext;

    public AuthService(Context context){
        mContext = context;
    }

    public int Login(String userName, String password, String DeviceId){
        int result;
        
        if(isLocalLoginDataPresent())
            result = validateLoginUsingLocalData(userName,password,DeviceId);
        else{
            SyncService mSyncService = new SyncService(mContext);
            boolean pullSuccess = mSyncService.pullCollectionAgentsAndSaveLocally();

            if(pullSuccess){
                if(isLocalLoginDataPresent())
                    result =  validateLoginUsingLocalData(userName, password, DeviceId);
                else
                    result = LoginResult.LOGIN_DATA_EMPTY;
            }
            else{
                result = LoginResult.COULD_NOT_ACCESS_SERVER;
            }
        }
        return result;
    }
    
    private boolean isLocalLoginDataPresent(){
        return CollectionAgent.isLocalDataPresent(mRealm);
    }

    private int validateLoginUsingLocalData(String userName, String password, String deviceId) {
        return CollectionAgent.validateAndLogin(userName, password, deviceId, mRealm);
    }

    public boolean Logout(){
        return CollectionAgent.Logout(mRealm);
    }

    public CollectionAgent getLoggedInAgent() {
        CollectionAgent agent = CollectionAgent.getLoggedInAgent(mRealm);
        return agent!=null? mRealm.copyFromRealm(agent): null;
    }

    public static class LoginResult{
        public static int VALID =1;
        public static int WRONG_DEVICEID = 2;
        public static int INVALID =3;
        public static int LOGIN_DATA_EMPTY=4;
        public static int COULD_NOT_ACCESS_SERVER=5;
    }
}
