package com.appfocus.vifi.services;

import com.appfocus.vifi.models.ApplicationSettings;

public class SettingsService extends BaseService {

    public ApplicationSettings getServerSettings(){
        ApplicationSettings applicationSettings = ApplicationSettings.get(mRealm);
        return applicationSettings !=null ? mRealm.copyFromRealm(applicationSettings) : null;
    }

    public boolean saveSettings(ApplicationSettings applicationSettings) {
        return ApplicationSettings.save(applicationSettings,mRealm);
    }
}
