package com.appfocus.vifi.services;

import android.content.Context;

import com.appfocus.vifi.models.ApplicationSettings;
import com.appfocus.vifi.models.CollectionAgent;
import com.appfocus.vifi.sqlconfig.SQLServer;

import java.sql.Connection;

public class TesterService extends BaseService {

    public void deleteAllLocalRecords(){
        mRealm.executeTransaction(realm -> realm.deleteAll());
    }

    public int getNoOfAgentsLoggedIn(){
        int result =-1;
        try {
            result= mRealm.where(CollectionAgent.class)
                    .equalTo("IsLoggedIn",true).findAll().size();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public void createInitialAppSettings(){
        ApplicationSettings.createInitialData(mRealm,true);
    }

    public boolean arrangeDataInServer(Context context) {
        boolean success = false;
        Connection connection;
        SQLServer mSQLServer;

        try {
            mSQLServer = new SQLServer(context);
            connection = mSQLServer.GetConnection();

            connection.prepareStatement( "delete from MobileReceipts").execute();
            connection.prepareStatement( "delete from CollectionAgents").execute();
            connection.prepareStatement( "insert into CollectionAgents values (1,'4279a88a57219cc8','ravikr','1234','A',0)").execute();
            connection.prepareStatement( "insert into CollectionAgents values(2,'413865d7c2bffd7e','jitu','1234','B',0)").execute();
            connection.prepareStatement( "insert into CollectionAgents values(3,'696dc5e99bbb5f77','appu','1234','C',0)").execute();

            success = true;
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return success;
    }
}
