package com.appfocus.vifi.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;

import com.appfocus.vifi.BuildConfig;

public class Device {

    public static Boolean isConnectedToWiFi(Context context){
        Boolean result = false;

        try {
            ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            result = mWifi.isConnected();
        }
        catch (Exception e) {
            e.printStackTrace();
            result = true; //benefit of doubt
        }
        return  result;
    }

    public static String GetDeviceID(Context context){
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String GetBuildVersionCode(){
        return String.valueOf(BuildConfig.VERSION_CODE) ;
    }

    public static boolean IsAppInDebugBuild(){
       return BuildConfig.DEBUG;
    }
}
