package com.appfocus.vifi.helpers;

import android.widget.EditText;

public class Validation {
    public static Boolean isInputGiven(EditText...ets){
        Boolean result = true;

        for(EditText et :ets) {
            if (et.getText().toString().trim().equalsIgnoreCase("")) {
                et.setError("Cannot be empty!");
                result = false;
            }
        }
        return result;
    }

    public static Boolean isNullOrEmpty(String str){
        return str == null || str.trim().isEmpty();
    }
}
