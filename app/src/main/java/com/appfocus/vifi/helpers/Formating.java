package com.appfocus.vifi.helpers;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Formating {

    public static String formatToRupees(double amt) {

        String strAmt;
        try{
            NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
            formatter.setMaximumFractionDigits(2);
            strAmt = formatter.format(amt);
        }
        catch (Exception ex){
            strAmt= Double.toString(amt);
        }
        return strAmt;
    }

    public static String toString_Indian_DateTime(Date date){
        String pattern = "dd-MM-yyyy HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
        String strDate = simpleDateFormat.format(date);
        return  strDate;
    }

    public static String toString_SQL_Server(Date date){
        //String pattern = "MM-dd-yyyy HH:mm:ss";
        String pattern = "dd-MMM-yyyy HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
        String strDate = simpleDateFormat.format(date);
        return  strDate;
    }

    public static String toString_TimeOnly(Date date){
        String pattern = "HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern,Locale.getDefault());
        String strDate = simpleDateFormat.format(date);
        return strDate;
    }
}
