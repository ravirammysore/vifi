package com.appfocus.vifi;

import android.app.Application;

import com.appfocus.vifi.realmconfig.AppInitData;
import com.appfocus.vifi.realmconfig.AppMigration;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Ravi on 23/02/2021.
 */

public class AppStart extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                //increment this after every change in schema...
                .schemaVersion(1)
                .allowQueriesOnUiThread(true)
                .allowWritesOnUiThread(true)
                .initialData(new AppInitData())
                //...and provide the migration object which has the logic for migration if we decide to have migrations
                .migration(new AppMigration())
                // *** UNCOMMENT ONLY FOR TESTING ***
                //.deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);
    }
}
