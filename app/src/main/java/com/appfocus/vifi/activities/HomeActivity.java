package com.appfocus.vifi.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.appfocus.vifi.R;
import com.appfocus.vifi.helpers.Device;
import com.google.android.material.snackbar.Snackbar;

public class HomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        wireUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id==R.id.action_main_settings){
            startActivity(new Intent(this,SettingsActivity.class));
            return true;
        }

        if(id==R.id.action_main_playstore){
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(
                    "https://play.google.com/store/apps/details?id=com.appfocus.vifi"));
            intent.setPackage("com.android.vending");
            startActivity(intent);
            return true;
        }

        if(id==R.id.action_main_logout){
            showToastLong("You are logged out");
            finishAndRemoveTask();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void btnLoanBalanceClicked(View view) {
        Intent intent = new Intent(this, LoanBalancesListActivity.class);
        startActivity(intent);
    }

    public void btnReceiptsClicked(View v){
        Intent intent = new Intent(this, ReceiptListActivity.class);
        startActivity(intent);
    }

    public void btnErrorsClicked(View v){
        Intent intent = new Intent(this, ErrorLogsListActivity.class);
        startActivity(intent);
    }

    CoordinatorLayout homeLayout;
    private void wireUI(){

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("ViFi Home");

        homeLayout = findViewById(R.id.homeLayout);
    }

    public void fabHomeClicked(View view) {
        Snackbar
                .make(homeLayout, "Application Build Version Code:"+ Device.GetBuildVersionCode(), Snackbar.LENGTH_LONG)
                .setAction("Help", v -> helpClicked())
                .show();
    }

    private void helpClicked() {
        Snackbar.make(homeLayout, "Help slated for next release", Snackbar.LENGTH_LONG).show();
    }

    public void btnSettingsClicked(View view) {
        startActivity(new Intent(this,SettingsActivity.class));
    }
}