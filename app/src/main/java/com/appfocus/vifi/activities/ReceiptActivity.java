package com.appfocus.vifi.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.preference.PreferenceManager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Layout;
import android.text.TextPaint;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.appfocus.vifi.R;
import com.appfocus.vifi.helpers.Formating;
import com.appfocus.vifi.helpers.Validation;
import com.appfocus.vifi.models.ErrorLog;
import com.appfocus.vifi.models.LoanBalance;
import com.appfocus.vifi.models.Receipt;
import com.appfocus.vifi.printers.NgxBTHelper;
import com.appfocus.vifi.services.AuthService;
import com.appfocus.vifi.services.LoanBalanceService;
import com.appfocus.vifi.services.ReceiptService;
import com.appfocus.vifi.services.SettingsService;
import com.google.android.material.snackbar.Snackbar;
import com.ngx.BluetoothPrinter;
import com.ngx.mp200sdk.NGXAshwaPrinter;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Logger;

public class ReceiptActivity extends BaseActivity {

    LoanBalance mLoanBalance;
    Receipt mReceipt;
    double depositAmount;

    //Ngx demo used static (so it could be accessed from any activity if required)
    //so keeping it that way even though this app does not access it from anywhere else
    public static BluetoothPrinter mBtp = BluetoothPrinter.INSTANCE;
    public static NGXAshwaPrinter mNgxPrinter = NGXAshwaPrinter.getNgxPrinterInstance();

    public static SharedPreferences mSP;
    private String mConnectedDeviceName = "";
    private boolean mUseBuiltInPrinter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);

        processActivity();
    }

    private void processActivity(){
        try{
            fetchLoan();
            prepareReceipt();

            wireUI();

            bindLedgerDetails();
            bindReceiptDetails();

            initializeAnyOnePrinterService();
        }
        catch (Exception ex){
            ErrorLog.LogError(ex);
            showToastShort("Error");
            finish();
        }
    }

    private void fetchLoan(){
        int loanNo = getIntent().getIntExtra("loanNo",0);

        LoanBalanceService loanBalanceService = new LoanBalanceService();
        mLoanBalance = loanBalanceService.getLoanByNo(loanNo);
    }

    private void prepareReceipt(){

        String receiptGUID = getIntent().getStringExtra("receiptGUID");

        if(areWeInCreateMode(receiptGUID)){
           createNewReceipt();
        }
        else{
            mReceipt = new ReceiptService(this).getReceipt(receiptGUID);
        }
    }

    private boolean areWeInCreateMode(String receiptGUID) {
        return Validation.isNullOrEmpty(receiptGUID);
    }

    private void createNewReceipt(){
        ReceiptService receiptService = new ReceiptService(this);
        String receiptCode;
        receiptCode = receiptService.getNextReceiptCode();
        Date todayDate = Calendar.getInstance().getTime();

        mReceipt = new Receipt();
        mReceipt.setLoanNo(mLoanBalance.getLoanNo());
        mReceipt.setReceiptCode(receiptCode);
        mReceipt.setReceiptGUID(UUID.randomUUID().toString());
        mReceipt.setReceiptDate(todayDate);
        mReceipt.setCreateTime(todayDate);
        mReceipt.setUpdateTime(todayDate);
    }

    String mLoanAmountStr, mPendingAmountStr,
            mTotalReceivedStr, mTodayPendingStr;
    String mLedgerNameStr, mLedgerGroupNameStr,
            mLoanFileNoStr, mLoanStatusStr;
    double mTodayPending;
    private void bindLedgerDetails(){

        mLedgerNameStr = mLoanBalance.getLedgerName();
        mLedgerGroupNameStr = mLoanBalance.getLedgerGroupName();
        mLoanFileNoStr = mLoanBalance.getFileNo();

        mLoanAmountStr = Formating.formatToRupees(mLoanBalance.getLoanAmount());
        mPendingAmountStr = Formating.formatToRupees(mLoanBalance.getPendingTillDate());
        mTotalReceivedStr = Formating.formatToRupees(mLoanBalance.getTotalReceived());
        mTodayPending = mLoanBalance.getPendingTillDate() - mLoanBalance.getTotalReceived();
        mTodayPendingStr = Formating.formatToRupees(mTodayPending);
        mLoanStatusStr = mLoanBalance.getStatus();

        tvLedgerName.setText(mLedgerNameStr);
        tvLedgerGroupName.setText(mLedgerGroupNameStr);
        tvFileNumber.setText(mLoanFileNoStr);

        tvLoanAmount.setText(mLoanAmountStr);
        tvPendingAmount.setText(mPendingAmountStr);
        tvTotalReceived.setText(mTotalReceivedStr);
        tvTodayPending.setText(mTodayPendingStr);
        tvLoanStatus.setText(mLoanStatusStr);

        tvDOF.setText(mLoanBalance.getDOF());
        tvDOE.setText(mLoanBalance.getDOE());
    }

    private void bindReceiptDetails(){
        tvReceiptCode.setText(mReceipt.getReceiptCode());
        tvReceiptDate.setText(Formating.toString_Indian_DateTime(mReceipt.getReceiptDate()));
        double amt = mReceipt.getReceiptAmt();
        if(amt>0) etReceiptAmount.setText(String.format("%.2f",amt));
        etReceiptComments.setText(mReceipt.getReceiptComments());
    }

    private void initializeAnyOnePrinterService() {
        mSP = PreferenceManager.getDefaultSharedPreferences(this);

        mUseBuiltInPrinter = new SettingsService().getServerSettings().getUseBuiltInPrinter();
        try {
            if(mUseBuiltInPrinter)
                mNgxPrinter.initService(this);
            else
                mBtp.initService(this, handler_btPrinter);
        }
        catch (Exception ex) {
            ErrorLog.LogError(ex);
            Toast.makeText(this, ex.getMessage().substring(0,30), Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("HandlerLeak")
    private final Handler handler_btPrinter = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BluetoothPrinter.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothPrinter.STATE_CONNECTED:
                            tvPrintStatus.setText(NgxBTHelper.title_connected_to);
                            tvPrintStatus.append(mConnectedDeviceName);
                            break;
                        case BluetoothPrinter.STATE_CONNECTING:
                            tvPrintStatus.setText(NgxBTHelper.title_connecting);
                            break;
                        case BluetoothPrinter.STATE_LISTEN:
                        case BluetoothPrinter.STATE_NONE:
                            tvPrintStatus.setText(NgxBTHelper.title_not_connected);
                            break;
                    }
                    break;
                case BluetoothPrinter.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(
                            BluetoothPrinter.DEVICE_NAME);
                    break;
                case BluetoothPrinter.MESSAGE_STATUS:
                    tvPrintStatus.setText(msg.getData().getString(
                            BluetoothPrinter.STATUS_TEXT));
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Deriving classes should always call through to the base implementation.
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.receipt_menu,menu);
        //as per google's documentation
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id ==  R.id.action_save_receipt) {
            if(isReceiptValid()) saveReceipt();
            return true;
        }

        if (id ==  R.id.action_clear_device) {
            NgxBTHelper.clearDevice(this);
            return true;
        }

        if (id == R.id.action_connect_device) {
            NgxBTHelper.showDeviceList(this);
            return true;
        }

        if (id == R.id.action_unpair_device) {
            NgxBTHelper.unpairDevice(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mBtp.onActivityResult(requestCode, resultCode, this);
    }

    @Override
    public void onDestroy() {
        mBtp.onActivityDestroy();
        super.onDestroy();
    }

    public void fabReceiptPrintClicked(View view) {
       printRRequestReceived();
    }

    private void printRRequestReceived(){
        if(isReceiptValid()) {
            try {
                saveReceipt();
                if(mUseBuiltInPrinter || isBluetoothPrinterReady())
                    composeAndPrintReceipt();
            }
            catch (Exception ex) {
                ErrorLog.LogError(ex);
                showToastShort("Error");
            }
        }
    }

    private boolean isReceiptValid(){
        boolean isValid = true;

        if(!Validation.isInputGiven(etReceiptAmount)) {
            isValid = false;
        }

        else {
            depositAmount = Double.parseDouble(etReceiptAmount.getText().toString());

            if(depositAmount<0) {
                Snackbar.make(layout, "Amount cannot be negative", Snackbar.LENGTH_LONG).show();
                isValid = false;
            }
        }
        return isValid;
    }

    private void saveReceipt() {
        boolean saved;

        mReceipt.setReceiptAmt(depositAmount);
        mReceipt.setReceiptComments(etReceiptComments.getText().toString());
        mReceipt.setUpdateTime(Calendar.getInstance().getTime());

        saved =  new ReceiptService(this).AddOrUpdateReceipt(mReceipt);

        if(!saved)
            showToastShort("Error in saving receipt");
        else
            showToastShort("Receipt saved");
    }

    private boolean isBluetoothPrinterReady(){
        boolean isReady= true;

        if(mBtp.getState() == BluetoothPrinter.PRINTER_PAPER_OUT){
            Snackbar snackbar = Snackbar
                    .make(layout, "Check paper and lid", Snackbar.LENGTH_LONG)
                    .setAction("connect", view1 -> NgxBTHelper.showDeviceList(ReceiptActivity.this));
            snackbar.show();
            isReady = false;
        }

        if (mBtp.getState() != BluetoothPrinter.STATE_CONNECTED) {
            Snackbar snackbar = Snackbar
                    .make(layout, "Printer is not connected", Snackbar.LENGTH_LONG)
                    .setAction("Connect", view1 -> NgxBTHelper.showDeviceList(ReceiptActivity.this));
            snackbar.show();
            isReady = false;
        }
        return isReady;
    }

    private void composeAndPrintReceipt() {
        int fontSizeTitle = 30;
        int fontSizeMatter = 24;
        int fontSizeFooter = 20;

        try {
            //region Declarations
            String separator = "---------------------------";
            Typeface typeface = Typeface.createFromAsset(ReceiptActivity.this.getAssets(), "fonts/DroidSansMono.ttf");
            TextPaint textPaint = new TextPaint();
            //endregion

            //region Title
            textPaint.setTypeface(Typeface.create(typeface, Typeface.BOLD));
            textPaint.setTextSize(fontSizeTitle);
            textPaint.setColor(Color.BLACK);

            addTextToPrint(textPaint, "Collection Receipt \n", Layout.Alignment.ALIGN_CENTER);
            //endregion

            //region Top
            textPaint.setTextSize(fontSizeMatter);
            textPaint.setTypeface(Typeface.create(typeface, Typeface.BOLD));

            StringBuilder builder = new StringBuilder();
            String strDate = Formating.toString_Indian_DateTime(mReceipt.getReceiptDate());
            String strAgentName = new AuthService(this).getLoggedInAgent().getUserName();
            String strCustomerName =mLoanBalance.getLedgerName();

            builder.append("Date: ").append(strDate).append("\n");
            builder.append("Collection Agent: ").append(strAgentName).append("\n");
            builder.append("Customer Name: ").append(strCustomerName).append("\n");
            builder.append("Receipt Code: ").append(mReceipt.getReceiptCode()).append("\n");
            builder.append("\n");

            //endregion

            //region Middle
            double receiptAmount = mReceipt.getReceiptAmt();
            String receiptAmountStr = Formating.formatToRupees(mReceipt.getReceiptAmt());

            double due = mTodayPending - receiptAmount;
            String dueStr = Formating.formatToRupees(due);

            builder.append(separator).append("\n");
            builder.append("File No: ").append(mLoanFileNoStr).append("\n");
            builder.append(separator).append("\n");

            builder.append("Loan Amount: ").append(mLoanAmountStr).append("\n");
            builder.append("Pending Amount:").append(mPendingAmountStr).append("\n");
            builder.append("Total Received:").append(mTotalReceivedStr).append("\n");
            builder.append("Today Pending:").append(mTodayPendingStr).append("\n");
            builder.append("Today Deposit: ").append(receiptAmountStr).append("\n");
            builder.append("\n");
            builder.append("Due:").append(dueStr).append("\n");
            builder.append("Status:").append(mLoanStatusStr).append("\n");
            builder.append("DOF:").append(mLoanBalance.getDOF()).append("\n");
            builder.append("DOE:").append(mLoanBalance.getDOE()).append("\n");
            builder.append(separator).append("\n");
            addTextToPrint(textPaint, builder.toString(), Layout.Alignment.ALIGN_NORMAL);
            //endregion

            //region Footer
            textPaint.setTextSize(fontSizeFooter);
            textPaint.setTypeface(Typeface.create(typeface, Typeface.BOLD));

            builder.setLength(0);
            builder.append("Developed by JSoft Mysore");
            builder.append("\n");
            addTextToPrint(textPaint, builder.toString(), Layout.Alignment.ALIGN_CENTER);
            //endregion

            fireThePrint();
        }
        catch (Exception ex)
        {
            ErrorLog.LogError(ex);
            String msg = ex.getMessage()==null?"Unknown Error":ex.getMessage();
            Snackbar.make(layout, msg, Snackbar.LENGTH_LONG).show();
        }
    }

    private void addTextToPrint(TextPaint textPaint, String s, Layout.Alignment alignCenter) {
        if(mUseBuiltInPrinter)
            mNgxPrinter.addText(s,alignCenter,textPaint);
        else
            mBtp.addText(s, alignCenter, textPaint);
    }

    private void fireThePrint(){
        int lineFeed = 6;
        try {
            if(mUseBuiltInPrinter){
                mNgxPrinter.print();
                mNgxPrinter.lineFeed(lineFeed);
            }
            else{
                mBtp.print();
                mBtp.FeedLine(lineFeed);
            }
        } catch (Exception e) {
            ErrorLog.LogError(e);
            Toast.makeText(this, "Error In Printing, See error logs!", Toast.LENGTH_SHORT).show();
        }
    }

    TextView tvReceiptCode,tvReceiptDate,
            tvLedgerName,tvLedgerGroupName, tvFileNumber,
            tvLoanAmount, tvPendingAmount, tvTotalReceived,
            tvTodayPending,tvLoanStatus, tvPrintStatus,
            tvDOF, tvDOE;

    EditText etReceiptAmount,etReceiptComments;
    CoordinatorLayout layout;


    private void wireUI(){

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("ViFi Receipt");

        tvReceiptCode = findViewById(R.id.tvReceiptCode);
        tvReceiptDate = findViewById(R.id.tvReceiptDate);

        etReceiptAmount = findViewById(R.id.etReceiptAmount);
        etReceiptComments = findViewById(R.id.etReceiptComments);

        tvLedgerName = findViewById(R.id.tvLedgerName);
        tvLedgerGroupName = findViewById(R.id.tvLedgerGroupName);
        tvFileNumber = findViewById(R.id.tvFileNumber);

        tvLoanAmount = findViewById(R.id.tvLoanAmount);
        tvPendingAmount = findViewById(R.id.tvPendingAmount);
        tvTotalReceived = findViewById(R.id.tvTotalReceived);
        tvTodayPending = findViewById(R.id.tvTodayPending);
        tvLoanStatus = findViewById(R.id.tvLoanStatus);

        tvPrintStatus = findViewById(R.id.tvPrintStatus);

        tvDOF = findViewById(R.id.tvDOF);
        tvDOE = findViewById(R.id.tvDOE);

        layout = findViewById(R.id.receiptLayout);
    }
}