package com.appfocus.vifi.activities;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Layout;
import android.text.TextPaint;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.preference.PreferenceManager;

import com.appfocus.vifi.R;
import com.appfocus.vifi.activities.adapters.ReceiptAdapter;
import com.appfocus.vifi.helpers.Formating;
import com.appfocus.vifi.models.ErrorLog;
import com.appfocus.vifi.models.Receipt;
import com.appfocus.vifi.printers.NgxBTHelper;
import com.appfocus.vifi.services.AuthService;
import com.appfocus.vifi.services.ReceiptService;
import com.appfocus.vifi.services.SettingsService;
import com.google.android.material.snackbar.Snackbar;
import com.ngx.BluetoothPrinter;
import com.ngx.mp200sdk.NGXAshwaPrinter;

import java.util.Calendar;
import java.util.List;

public class ReceiptListActivity extends BaseActivity implements SearchView.OnQueryTextListener {

    List<Receipt> mLstModelItems;

    //Ngx demo used static (so it could be accessed from any activity if required)
    //so keeping it that way even though this app does not access it from anywhere else
    public static BluetoothPrinter mBtp = BluetoothPrinter.INSTANCE;
    public static NGXAshwaPrinter mNgxPrinter = NGXAshwaPrinter.getNgxPrinterInstance();

    public static SharedPreferences mSP;
    private String mConnectedDeviceName = "";
    private boolean mUseBuiltInPrinter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_list);

        mLstModelItems = new ReceiptService(this).getReceipts();

        wireUI();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        try {
            mModelAdapter.getFilter().filter(newText);
        } catch (Exception ex) {
            showToastLong(ex.getMessage());
            return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Deriving classes should always call through to the base implementation.
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.receipts_list_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(false);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("enter amount to search");
        //as per google's documentation
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mModelAdapter!=null)
            reloadList();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id ==  R.id.action_print_total) {
            printTotalRequested();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void printTotalRequested() {
        try {
            initializeAnyOnePrinterService();
            if(mUseBuiltInPrinter || isBluetoothPrinterReady())
                composeAndPrintTotal();
        }
        catch (Exception ex){
            ErrorLog.LogError(ex);
            showToastShort("Error");
        }
    }

    private boolean isBluetoothPrinterReady(){
        boolean isReady= true;

        if(mBtp.getState() == BluetoothPrinter.PRINTER_PAPER_OUT){
            Snackbar snackbar = Snackbar
                    .make(layout, "Check paper and lid", Snackbar.LENGTH_LONG)
                    .setAction("connect", view1 -> NgxBTHelper.showDeviceList(ReceiptListActivity.this));
            snackbar.show();
            isReady = false;
        }

        if (mBtp.getState() != BluetoothPrinter.STATE_CONNECTED) {
            Snackbar snackbar = Snackbar
                    .make(layout, "Printer is not connected", Snackbar.LENGTH_LONG)
                    .setAction("Connect", view1 -> NgxBTHelper.showDeviceList(ReceiptListActivity.this));
            snackbar.show();
            isReady = false;
        }
        return isReady;
    }
    private void composeAndPrintTotal() {

        int noOfReceipts = mLstModelItems.size();

        double totalAmount=0.0;
        for (Receipt receipt: mLstModelItems) totalAmount += receipt.getReceiptAmt();

        int fontSizeTitle = 30;
        int fontSizeMatter = 24;
        int fontSizeFooter = 20;

        try {
            //region Declarations
            String _separator_ = "---------------------------";
            Typeface typeface = Typeface.createFromAsset(ReceiptListActivity.this.getAssets(), "fonts/DroidSansMono.ttf");
            TextPaint textPaint = new TextPaint();
            //endregion

            //region Title
            textPaint.setTypeface(Typeface.create(typeface, Typeface.BOLD));
            textPaint.setTextSize(fontSizeTitle);
            textPaint.setColor(Color.BLACK);

            addTextToPrint(textPaint, "Receipts Total \n", Layout.Alignment.ALIGN_CENTER);
            //endregion

            //region Body
            textPaint.setTextSize(fontSizeMatter);
            textPaint.setTypeface(Typeface.create(typeface, Typeface.BOLD));

            StringBuilder builder = new StringBuilder();
            String strDate = Formating.toString_Indian_DateTime(Calendar.getInstance().getTime());
            String strAgentName = new AuthService(this).getLoggedInAgent().getUserName();

            builder.append("Date: ").append(strDate).append("\n");
            builder.append("Collection Agent: ").append(strAgentName).append("\n");
            builder.append("No Of Receipts: ").append(noOfReceipts).append("\n");
            builder.append(_separator_).append("\n");
            builder.append("Receipts Total Amount: ").append(Formating.formatToRupees(totalAmount)).append("\n");
            builder.append(_separator_).append("\n");
            builder.append("\n");

            addTextToPrint(textPaint, builder.toString(), Layout.Alignment.ALIGN_NORMAL);

            //endregion

            //region Footer
            textPaint.setTextSize(fontSizeFooter);
            textPaint.setTypeface(Typeface.create(typeface, Typeface.BOLD));

            builder.setLength(0);
            builder.append("Developed by JSoft Mysore");
            builder.append("\n");
            addTextToPrint(textPaint, builder.toString(), Layout.Alignment.ALIGN_CENTER);
            //endregion

            fireThePrint();
        }
        catch (Exception ex)
        {
            ErrorLog.LogError(ex);
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void initializeAnyOnePrinterService() {
        mSP = PreferenceManager.getDefaultSharedPreferences(this);

        mUseBuiltInPrinter = new SettingsService().getServerSettings().getUseBuiltInPrinter();
        try {
            if(mUseBuiltInPrinter)
                mNgxPrinter.initService(this);
            else
                mBtp.initService(this, handler_btPrinter);
        }
        catch (Exception ex) {
            ErrorLog.LogError(ex);
            Toast.makeText(this, ex.getMessage().substring(0,20), Toast.LENGTH_SHORT).show();
        }
    }

    private void addTextToPrint(TextPaint textPaint, String contentToPrint, Layout.Alignment alignCenter) {
        if(mUseBuiltInPrinter)
            mNgxPrinter.addText(contentToPrint,alignCenter,textPaint);
        else
            mBtp.addText(contentToPrint, alignCenter, textPaint);
    }

    private void fireThePrint() {
        int lineFeed = 6;
        try {
            if (mUseBuiltInPrinter) {
                mNgxPrinter.print();
                mNgxPrinter.lineFeed(lineFeed);
            } else {
                mBtp.print();
                mBtp.FeedLine(lineFeed);
            }
        } catch (Exception e) {
            ErrorLog.LogError(e);
            Toast.makeText(this, "Error In Printing, See error logs!", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("HandlerLeak")
    private final Handler handler_btPrinter = new Handler() {
        @Override
        public void handleMessage(Message msg) {
           //to do later if required to show status changes of BT, otherwise ignore this
            /*switch (msg.what) {
                case BluetoothPrinter.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothPrinter.STATE_CONNECTED:
                            tvPrintStatus.setText(NgxBTHelper.title_connected_to);
                            tvPrintStatus.append(mConnectedDeviceName);
                            break;
                        case BluetoothPrinter.STATE_CONNECTING:
                            tvPrintStatus.setText(NgxBTHelper.title_connecting);
                            break;
                        case BluetoothPrinter.STATE_LISTEN:
                        case BluetoothPrinter.STATE_NONE:
                            tvPrintStatus.setText(NgxBTHelper.title_not_connected);
                            break;
                    }
                    break;
                case BluetoothPrinter.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(
                            BluetoothPrinter.DEVICE_NAME);
                    break;
                case BluetoothPrinter.MESSAGE_STATUS:
                    tvPrintStatus.setText(msg.getData().getString(
                            BluetoothPrinter.STATUS_TEXT));
                    break;
                default:
                    break;
            }*/
        }
    };

    public void fabPushReceiptsClicked(View view) {
        if(mLstModelItems.size()==0)
            Snackbar.make(layout, "No records to transfer", Snackbar.LENGTH_LONG).show();
        else
            Snackbar
                    .make(layout, "Transfer all receipts?", Snackbar.LENGTH_LONG)
                    .setAction("Yes", v -> pushReceipts())
                    .show();
    }

    private void pushReceipts() {
        Snackbar.make(layout, "Please wait..", Snackbar.LENGTH_LONG).show();

        mExecutorService.execute(() -> {
            boolean done =  new ReceiptService(ReceiptListActivity.this).push_update_clear();
            mHandler.post(() -> {
                if(done){
                    showToastShort("Receipts transferred");
                    finish();
                }
                else{
                    Snackbar
                            .make(layout, "Could not transfer receipts", Snackbar.LENGTH_LONG)
                            .setAction("Show Errors", v -> startActivity(new Intent(this,ErrorLogsListActivity.class)))
                            .show();
                }
            });
        });
    }

    private void reloadList(){
        mLstModelItems = new ReceiptService(this).getReceipts();
        mModelAdapter = new ReceiptAdapter(this,mLstModelItems);
        listViewModelItems.setAdapter(mModelAdapter);
    }

    CoordinatorLayout layout;
    ListView listViewModelItems;
    ReceiptAdapter mModelAdapter;

    private void wireUI() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("ViFi Receipts");

        layout = findViewById(R.id.receiptsListLayout);
        listViewModelItems = findViewById(R.id.listViewReceipts);

        mModelAdapter = new ReceiptAdapter(this,mLstModelItems);
        listViewModelItems.setAdapter(mModelAdapter);

        listViewModelItems.setOnItemClickListener((parent, view, position, id) -> {
            Receipt modelItem = (Receipt) parent.getItemAtPosition(position);
            Intent intent = new Intent(ReceiptListActivity.this, ReceiptActivity.class)
                    .putExtra("loanNo",modelItem.getLoanNo())
                    .putExtra("receiptGUID",modelItem.getReceiptGUID());

            startActivity(intent);
        });

        if(mLstModelItems.size()==0)
            Snackbar.make(layout, "No records", Snackbar.LENGTH_LONG).show();
    }
}
