package com.appfocus.vifi.activities;

import android.content.Intent;
import android.os.Bundle;

import com.appfocus.vifi.helpers.Device;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.appfocus.vifi.R;
import com.appfocus.vifi.helpers.Validation;
import com.appfocus.vifi.services.AuthService;
import com.appfocus.vifi.services.SyncService;
import com.google.android.material.snackbar.Snackbar;

public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        wireUI();

        if(Device.IsAppInDebugBuild()){
            etLoginUserName.setText("chandru");
            etLoginPassword.setText("1234");
        }

        tvDeviceID.setText("Device ID: "+Device.GetDeviceID(this));
        tvAppBuildVersionCode.setText("Application Build Version Code:"+ Device.GetBuildVersionCode());
    }

    public void fabLoginDownloadClicked(View view) {
        Snackbar.make(loginLayout, "downloading...", Snackbar.LENGTH_LONG).show();

        mExecutorService.execute(() -> {
            Boolean success =  new SyncService(LoginActivity.this).pullCollectionAgentsAndSaveLocally();
            mHandler.post(() -> {
                if(success)
                    Snackbar.make(loginLayout, "download complete", Snackbar.LENGTH_LONG).show();
                else
                    Snackbar.make(loginLayout, "could not download", Snackbar.LENGTH_LONG).show();
            });
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.login_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if(id==R.id.action_main_settings){
            startActivity(new Intent(this,SettingsActivity.class));
            return true;
        }
        if(id==R.id.action_main_playstore){
            startActivity(new Intent(this,ErrorLogsListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void btnLoginClicked(View view) {

        if(!Validation.isInputGiven(etLoginUserName,etLoginPassword))
            return;

        showToastShort("Please wait...");

        mExecutorService.execute(() -> {
            int loginResult = new AuthService(LoginActivity.this).Login(etLoginUserName.getText().toString(),
                    etLoginPassword.getText().toString(),
                    Device.GetDeviceID(LoginActivity.this));

            mHandler.post(() -> {
                if(loginResult == AuthService.LoginResult.VALID) {
                    startActivity(new Intent(LoginActivity.this,HomeActivity.class));
                    finish();
                }
                else if (loginResult == AuthService.LoginResult.INVALID)
                    showToastShort("Invalid credentials");

                else if (loginResult == AuthService.LoginResult.LOGIN_DATA_EMPTY)
                    showToastShort("Login data empty");

                else if (loginResult == AuthService.LoginResult.WRONG_DEVICEID)
                    showToastShort("Device Id not matching");

                else if (loginResult == AuthService.LoginResult.COULD_NOT_ACCESS_SERVER)
                    showToastShort("Could not get data from server");
                else
                    showToastShort("Error logging in");
            });
        });
    }

    public void tvDeviceIdClicked(View view) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, tvDeviceID.getText());
        sendIntent.setType("text/plain");

        Intent shareIntent = Intent.createChooser(sendIntent, null);
        startActivity(shareIntent);
    }

    TextView tvDeviceID, tvAppBuildVersionCode;
    EditText etLoginUserName, etLoginPassword;
    CoordinatorLayout loginLayout;

    private void wireUI() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("ViFi Login");

        tvDeviceID = findViewById(R.id.tvDeviceID);
        tvAppBuildVersionCode = findViewById(R.id.tvAppBuildVersionCode);
        etLoginUserName = findViewById(R.id.etLoginUserName);
        etLoginPassword = findViewById(R.id.etLoginPassword);
        loginLayout = findViewById(R.id.loginLayout);
    }

    public void tvVersionClicked(View view) {
        //startActivity(new Intent(this,PrintingActivity.class));
    }
}