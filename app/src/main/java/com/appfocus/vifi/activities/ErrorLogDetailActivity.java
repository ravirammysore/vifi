package com.appfocus.vifi.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import com.appfocus.vifi.models.ErrorLog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.appfocus.vifi.R;

public class ErrorLogDetailActivity extends AppCompatActivity {

    ErrorLog mErrorLog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_log_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("ViFi Error Detail");

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> shareErrorLog());

        String Id = getIntent().getStringExtra("Id");
        mErrorLog = ErrorLog.Get(Id);

        TextView tvErrorMessage = findViewById(R.id.tvErrorMessage);
        TextView tvStackTrace = findViewById(R.id.tvStackTrace);
        tvStackTrace.setMovementMethod(new ScrollingMovementMethod());

        tvErrorMessage.setText(mErrorLog.getErrorMessage());
        tvStackTrace.setText(mErrorLog.getStackTrace());
    }

    private void shareErrorLog() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, mErrorLog.getErrorMessage()+"\n"+ mErrorLog.getStackTrace());
        sendIntent.setType("text/plain");

        Intent shareIntent = Intent.createChooser(sendIntent, "Share Error Detail");
        startActivity(shareIntent);
    }
}