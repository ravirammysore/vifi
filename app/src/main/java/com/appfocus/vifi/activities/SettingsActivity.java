package com.appfocus.vifi.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.appfocus.vifi.R;
import com.appfocus.vifi.helpers.Validation;
import com.appfocus.vifi.models.ApplicationSettings;
import com.appfocus.vifi.models.ErrorLog;
import com.appfocus.vifi.services.SettingsService;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.switchmaterial.SwitchMaterial;

public class SettingsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        processActivity();
    }

    private void processActivity(){
        try{
            wireControls();
            showSettings();
        }
        catch(Exception ex){
            ErrorLog.LogError(ex);
            showToastShort("Error");
            finish();
        }
    }

    private void showSettings() {
        ApplicationSettings applicationSettings;
        applicationSettings = new SettingsService().getServerSettings();

        if(applicationSettings ==null) return;

        etServerIP.setText(applicationSettings.getServerIPAddress());
        etDbName.setText(applicationSettings.getDatabaseName());
        etDbUserName.setText(applicationSettings.getDatabaseUserName());
        etDBPassword.setText(applicationSettings.getDatabasePassword());
        switchPrinterSelect.setChecked(applicationSettings.getUseBuiltInPrinter());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Deriving classes should always call through to the base implementation.
        super.onCreateOptionsMenu(menu);
        //getMenuInflater().inflate(R.menu.settings_menu,menu);
        //as per google's documentation
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_restore_original) {
            showToastShort("restore to do");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveSettings(){

        if(!Validation.isInputGiven(etServerIP,etDbName,etDbUserName,etDBPassword)) return;

        ApplicationSettings applicationSettings = new ApplicationSettings(
                etServerIP.getText().toString(),
                etDbName.getText().toString(),
                etDbUserName.getText().toString(),
                etDBPassword.getText().toString(),
                switchPrinterSelect.isChecked()
        );

        if(new SettingsService().saveSettings(applicationSettings))
            Snackbar.make(settingsLayout, "Saved", Snackbar.LENGTH_LONG).show();
        else
            Snackbar.make(settingsLayout, "Could not save", Snackbar.LENGTH_LONG).show();
        finish();
    }

    EditText etServerIP, etDbName, etDbUserName, etDBPassword;
    CoordinatorLayout settingsLayout;
    SwitchMaterial switchPrinterSelect;

    private void wireControls(){

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("ViFi Settings");

        settingsLayout = findViewById(R.id.settingsLayout);

        etServerIP = findViewById(R.id.etServerIP);
        etDbName = findViewById(R.id.etDbName);
        etDbUserName = findViewById(R.id.etDbUserName);
        etDBPassword = findViewById(R.id.etDbPassword);

        switchPrinterSelect = findViewById(R.id.switchPrinterSelect);
    }

    public void fabSettingsSaveClicked(View view) {
        saveSettings();
    }

    public void tvLoadDeveloperSettingsClicked(View view) {
        etServerIP.setText("192.168.1.103");
        etDbName.setText("VIFISQL");
        etDbUserName.setText("sa");
        etDBPassword.setText("ravi1234#");
    }
}