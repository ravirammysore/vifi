package com.appfocus.vifi.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.appfocus.vifi.R;
import com.appfocus.vifi.sqlconfig.SQLServer;

import java.sql.Connection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.realm.Realm;

public abstract class BaseActivity extends AppCompatActivity {

    ExecutorService mExecutorService;
    ProgressDialog progressDialog;
    Handler mHandler;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = (View) findViewById(android.R.id.content);
        rootView.setBackground(getResources().getDrawable(R.drawable.back2));

        mExecutorService = Executors.newFixedThreadPool(10);
        mHandler = new Handler(Looper.getMainLooper());

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Processing");
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
    }

    protected void showProgressDialogue(){
        if(!progressDialog.isShowing())
            progressDialog.show();
    }

    protected void hideProgressDialogue(){
        if(progressDialog.isShowing())
            progressDialog.dismiss();
    }

    protected void showToastShort(String message){
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    protected void showToastLong(String message){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {

        if(!mExecutorService.isShutdown())
            mExecutorService.shutdown();

        super.onDestroy();
    }
}
