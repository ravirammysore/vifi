package com.appfocus.vifi.activities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import com.appfocus.vifi.helpers.Formating;

public class GenericAdapter extends BaseAdapter implements Filterable {

    private final List<Object> lstModels;
    private List<Object> lstModelsFiltered;
    private ModelFilter modelFilter;

    private final LayoutInflater layoutInflater;

    public GenericAdapter(Context context, List<Object> items) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        lstModels = items;
        lstModelsFiltered = items;
    }

    @Override
    public int getCount() {
        return lstModelsFiltered.size();
    }

    @Override
    public Object getItem(int position) {
        return lstModelsFiltered.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position + 1;
    }

    @Override
    public View getView(int position, View model_item_view, ViewGroup parent) {

        if (model_item_view == null) {
            //This is how the adapter gets to know which template to use for showing an item in the collection - ravi
            model_item_view = layoutInflater.inflate(android.R.layout.simple_list_item_2, null);
        }
        TextView tvText1 = (TextView) model_item_view.findViewById(android.R.id.text1);
        TextView tvText2 = (TextView) model_item_view.findViewById(android.R.id.text2);

        try {
            String lineOne=null, lineTwo=null;
            Object modelItem = lstModelsFiltered.get(position);

            //lineOne = String.format("%s (%s)", modelItem.getReceiptNo(), modelItem.getReceiptDate());
            tvText1.setText(lineOne);

            //lineTwo = String.format("Amount: %s", Formating.formatToRupees(modelItem.getReceiptAmt()));
            tvText2.setText(lineTwo);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return model_item_view;
    }

    @Override
    public Filter getFilter() {
        if (modelFilter == null) {
            modelFilter = new ModelFilter();
        }
        return modelFilter;
    }

    //Nested class, for easy access of container class members
    private class ModelFilter extends Filter {

        //This method runs in background thread and publishes result to foreground thread
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint!=null && constraint.length()>0) {

                List<Object> tempList = new ArrayList<>();
                String searchText = constraint.toString().toLowerCase();
                String strSearchProperty;
                boolean matchFound;

                for (Object modelItem : lstModels) {
                    //strSearchProperty = modelItem.getReceiptAmt().toString();
                    //matchFound = strSearchProperty.contains(searchText);
                    //if (matchFound) tempList.add(modelItem);
                }
                results.count = tempList.size();
                results.values = tempList;
            }
            else {
                //when no constraint is given, our original list itself will be the result
                results.count = lstModels.size();
                results.values = lstModels;
            }
            return results;
        }
        /* This function receives results from background thread*/
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            lstModelsFiltered = (List<Object>) results.values;
            //this is standard stuff as per android listview filtering process
            notifyDataSetChanged();
        }
    }
}


