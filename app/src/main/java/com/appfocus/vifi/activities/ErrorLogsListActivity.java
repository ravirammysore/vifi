package com.appfocus.vifi.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.appfocus.vifi.R;
import com.appfocus.vifi.activities.adapters.ErrorLogAdapter;
import com.appfocus.vifi.activities.adapters.GenericAdapter;
import com.appfocus.vifi.models.ErrorLog;

import java.util.List;

public class ErrorLogsListActivity extends BaseActivity implements SearchView.OnQueryTextListener {

    List<ErrorLog> mLstModelItems;
    ErrorLogAdapter mModelAdapter;
    ListView listViewModelItems;
    //Service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_logs_list);

        mLstModelItems = ErrorLog.GetAll();

        wireUI();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        try {
            mModelAdapter.getFilter().filter(newText);
        } catch (Exception ex) {
            showToastLong(ex.getMessage());
            return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Deriving classes should always call through to the base implementation.
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.error_logs_list_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(false);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("search by error message");
        //as per google's documentation
        return true;
    }

    CoordinatorLayout coordinatorLayout;

    private void wireUI() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("ViFi Error Logs");

        coordinatorLayout = findViewById(R.id.errorListCoordinatorLayout);

        listViewModelItems = findViewById(R.id.lvErrorLogs);

        mModelAdapter = new ErrorLogAdapter(this, mLstModelItems);
        listViewModelItems.setAdapter(mModelAdapter);

        listViewModelItems.setOnItemClickListener((parent, view, position, id) -> {
            ErrorLog modelItem = (ErrorLog) parent.getItemAtPosition(position);
            Intent intent = new Intent(ErrorLogsListActivity.this, ErrorLogDetailActivity.class)
                    .putExtra("Id",modelItem.getId());
            startActivity(intent);
        });
    }
}
