package com.appfocus.vifi.activities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import com.appfocus.vifi.models.LoanBalance;

public class LoanBalanceAdapter extends BaseAdapter implements Filterable {

    private final List<LoanBalance> lstLoanBalances;

    private final LayoutInflater layoutInflater;

    private List<LoanBalance> lstLoanBalancesFiltered;

    private LoanBalanceAdapter.LoanBalanceFilter loanBalanceFilter;

    public LoanBalanceAdapter(Context context, List<LoanBalance> items) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        lstLoanBalances = items;
        lstLoanBalancesFiltered = items;
    }

    @Override
    public int getCount() {
        //return lstLoanBalances.size();
        return lstLoanBalancesFiltered.size();
    }

    @Override
    public Object getItem(int position) {
        //return lstLoanBalances.get(position);
        return lstLoanBalancesFiltered.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position + 1;
    }

    @Override
    public View getView(int position, View Loan_balance_view, ViewGroup parent) {

        if (Loan_balance_view == null) {
            //This is how the adapter gets to know which template to use for showing an item in the collection - ravi
            Loan_balance_view = layoutInflater.inflate(android.R.layout.simple_list_item_2, null);
        }
        TextView tvText1 = (TextView) Loan_balance_view.findViewById(android.R.id.text1);
        TextView tvText2 = (TextView) Loan_balance_view.findViewById(android.R.id.text2);

        try {
            String lineOne, lineTwo;
            LoanBalance loanBalance = lstLoanBalancesFiltered.get(position);

            lineOne = String.format("%s (%s)", loanBalance.getLedgerName(), loanBalance.getLedgerGroupName());
            tvText1.setText(lineOne);

            lineTwo = String.format("File Number: %s", loanBalance.getFileNo());
            tvText2.setText(lineTwo);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return Loan_balance_view;
    }

    @Override
    public Filter getFilter() {
        if (loanBalanceFilter == null) {
            loanBalanceFilter = new LoanBalanceFilter();
        }

        return loanBalanceFilter;
    }

    //Nested class, for easy access of container class members
    private class LoanBalanceFilter extends Filter {
        //This method runs in background thread and publishes result to foreground thread
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            if (constraint!=null && constraint.length()>0) {

                List<LoanBalance> tempList = new ArrayList<>();
                String ledgerName, fileNo, searchText;

                searchText = constraint.toString().toLowerCase();

                for (LoanBalance loanBalance : lstLoanBalances) {

                    ledgerName = loanBalance.getLedgerName().toLowerCase();
                    fileNo = loanBalance.getFileNo();

                    boolean matchFound = ledgerName.contains(searchText) || fileNo.contains(searchText);
                    if (matchFound)
                        tempList.add(loanBalance);
                }
                results.count = tempList.size();
                results.values = tempList;
            }
            else {
                //when no constraint is given, our original list itself will be the result
                results.count = lstLoanBalances.size();
                results.values = lstLoanBalances;
            }
            return results;
        }
        /* This function receives results from background thread*/
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            lstLoanBalancesFiltered = (List<LoanBalance>) results.values;
            //this is standard stuff as per android listview filtering process
            notifyDataSetChanged();
        }
    }
}


