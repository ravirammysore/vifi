package com.appfocus.vifi.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.appfocus.vifi.R;
import com.appfocus.vifi.activities.adapters.GenericAdapter;

import java.util.List;

public class GenericListActivity extends BaseActivity implements SearchView.OnQueryTextListener {

    List<Object> mLstModelItems;
    GenericAdapter mModelAdapter;
    ListView listViewModelItems;
    //Service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setContentView(R.layout.YOUR_ACTIVITY);

        //Service = new Service();
        //lstModelItems = service.getModelItems

        wireUI();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        try {
            mModelAdapter.getFilter().filter(newText);
        } catch (Exception ex) {
            showToastLong(ex.getMessage());
            return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Deriving classes should always call through to the base implementation.
        super.onCreateOptionsMenu(menu);

        //getMenuInflater().inflate(R.menu.activity_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(false);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("enter search text");
        //as per google's documentation
        return true;
    }

    public void fabClicked(View view) {
        //your code
    }

    CoordinatorLayout coordinatorLayout;

    private void wireUI() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //coordinatorLayout = findViewById(R.id.yourCoordinatorLayout);

        listViewModelItems = findViewById(R.id.listViewReceipts);

        mModelAdapter = new GenericAdapter(this, mLstModelItems);
        listViewModelItems.setAdapter(mModelAdapter);

        listViewModelItems.setOnItemClickListener((parent, view, position, id) -> {
            Object modelItem = (Object) parent.getItemAtPosition(position);
            //Intent intent = new Intent(ThisActivity.this, NextActivity.class).putExtra("loanNo",modelItem.getProperty());
            //startActivity(intent);
        });
    }
}
