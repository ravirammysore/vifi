package com.appfocus.vifi.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;

import com.appfocus.vifi.R;
import com.google.android.material.snackbar.Snackbar;

import java.lang.reflect.Field;
import java.util.List;

import com.appfocus.vifi.activities.adapters.LoanBalanceAdapter;
import com.appfocus.vifi.models.LoanBalance;
import com.appfocus.vifi.services.LoanBalanceService;
import com.appfocus.vifi.services.SyncService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class LoanBalancesListActivity extends BaseActivity implements SearchView.OnQueryTextListener  {

    List<LoanBalance> mLstModelItems;
    LoanBalanceAdapter mModelAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loanbalanceslist);

        mLstModelItems = new LoanBalanceService().getLoanBalances();

        wireUI();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        try {
            mModelAdapter.getFilter().filter(newText);
        } catch (Exception ex) {
            showToastLong(ex.getMessage());
            return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Deriving classes should always call through to the base implementation.
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.loan_balances_list_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(false);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("enter name or ref no.");

        //not working - ravikr
        //changeSearchCursorColor(searchView);

        //as per google's documentation
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id ==  R.id.action_delete_loans) {
            deleteLoanRecordsInMobile();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteLoanRecordsInMobile(){
        boolean deleted;

        deleted =  new LoanBalanceService().deleteLoanRecordsinMobile();

        if(!deleted)
            showToastShort("Error in deleting");
        else {
            showToastShort("Deleted");
            finish();
        }
    }
    private void changeSearchCursorColor(SearchView searchView) {
        EditText etSearch= ((EditText) searchView.findViewById(R.id.search_src_text));
        try {
            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(etSearch, null);// set textCursorDrawable to null
        } catch (Exception e) {
            e.printStackTrace();
        }
        etSearch.setTextColor(ContextCompat.getColor(LoanBalancesListActivity.this, R.color.white));
    }

    @Override
    protected void onResume() {
        super.onResume();
        //reloadList();
    }

    public void fabDownloadLedgerClicked(View view) {
        downloadLedger();
    }

    private void downloadLedger() {
        progressBarLoans.setVisibility(ProgressBar.VISIBLE);

        mExecutorService.execute(() -> {
            SyncService syncService = new SyncService(LoanBalancesListActivity.this);
            Boolean ledgersPulled = syncService.pullLoanBalancesAndSaveLocally();

            mHandler.post(() -> {
                progressBarLoans.setVisibility(ProgressBar.GONE);
                if(ledgersPulled) {
                    showToastShort("Download success");
                    finish();
                } else
                    Snackbar
                        .make(ledgerCoordinatorLayout, "Could not download", Snackbar.LENGTH_LONG)
                        .setAction("Show Errors", v -> startActivity(new Intent(this,ErrorLogsListActivity.class)))
                        .show();
            });
        });
    }

    //does not work well, so closing the activity instead
    private void reloadList(){
        mExecutorService.execute(() -> {
            mLstModelItems = new LoanBalanceService().getLoanBalances();
            mHandler.post(() -> {
                mModelAdapter = new LoanBalanceAdapter(this, mLstModelItems);
                listViewModelItems.setAdapter(mModelAdapter);
            });
        });
    }

    FloatingActionButton fabDownloadLedger;
    CoordinatorLayout ledgerCoordinatorLayout;
    ListView listViewModelItems;
    ProgressBar progressBarLoans;

    private void wireUI(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("ViFi Loans List");

        progressBarLoans = findViewById(R.id.progressBarLoans);
        fabDownloadLedger = findViewById(R.id.fabDownloadLedger);
        ledgerCoordinatorLayout = findViewById(R.id.ledgerCoordinatorLayout);

        listViewModelItems = findViewById(R.id.lvLoanBalances);
        mModelAdapter = new LoanBalanceAdapter(this, mLstModelItems);
        listViewModelItems.setAdapter(mModelAdapter);

        listViewModelItems.setOnItemClickListener((parent, view, position, id) -> {
            LoanBalance loanBalance = (LoanBalance) parent.getItemAtPosition(position);
            Intent intent = new Intent(LoanBalancesListActivity.this, ReceiptActivity.class)
                    .putExtra("loanNo",loanBalance.getLoanNo());
            startActivity(intent);
        });

        if(mLstModelItems.size()==0)
            Snackbar
                    .make(ledgerCoordinatorLayout, "No items", Snackbar.LENGTH_LONG)
                    .setAction("Download", v -> downloadLedger())
                    .show();
    }
}