package com.appfocus.vifi.realmconfig;

import com.appfocus.vifi.models.ApplicationSettings;

import io.realm.Realm;

public class AppInitData implements Realm.Transaction {
    @Override
    public void execute(Realm realm) {
        ApplicationSettings.createInitialData(realm,false);
    }
}
