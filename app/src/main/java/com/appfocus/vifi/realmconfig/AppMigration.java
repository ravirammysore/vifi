package com.appfocus.vifi.realmconfig;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

/**
 * Created by User on 13-08-2017.
 */

public class AppMigration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema realmSchema = realm.getSchema();

        // ** IN CASE OF ANY SCHEMA CHANGES, DO NOT FORGET TO INCREMENT SCHEMA NUMBER MANUALLY IN realmConfiguration IN APPSTART.JAVA **

        //first migration
        if(oldVersion==0){
            realmSchema.get("LoanBalance").addField("DOF",String.class);
            realmSchema.get("LoanBalance").addField("DOE",String.class);
            oldVersion++;
        }

        //for our second migration in future
        if(oldVersion==1){
            /*realmSchema.get("Contact").addField("city",String.class);
            oldVersion++;*/
        }

        //and so on, for every migration
    }
}
