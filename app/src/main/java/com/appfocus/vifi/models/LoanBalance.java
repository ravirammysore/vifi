package com.appfocus.vifi.models;

import android.widget.Toast;

import com.appfocus.vifi.helpers.Validation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

public class LoanBalance extends RealmObject {

    @PrimaryKey
    private int LoanNo;
    private String FileNo;
    private String LedgerName;
    private String LedgerGroupName;
    private String BrokerName;
    private String GroupName;
    private double LoanAmount;
    private double PendingTillDate;
    private double TotalReceived;
    private double Balance;
    private double PaidAmount;
    private double TotalInstallAmount;
    private String Status;
    private String RefNo;
    private int LedgerId;
    private int LedgerGroupId;
    private double PendingAmount;
    private String DOF;
    private String DOE;

    public static Boolean pullLoanBalancesAndSaveLocally(Connection connection, Realm realm) {
        Boolean pullAndSaved = false;

        if(connection!=null){

            //region Get loan query
            //Not using hardcoded query anymore
            /* String sqlQry = "SELECT LoanNo,RefNo as FileNo,LedgerName,LedgerGroupName,BrokerName, GroupName, LoanAmount,PendingTillDate, TotalReceived, \n" +
                    "PendingTillDate - TotalReceived as Balance,  Format(FStartDate, 'dd/MM/yy') as FS,\n" +
                    "format(FEndDate, 'dd/MM/yy') as FE, PaidAmt ,IIF(PaidAmt > 0, 'Paid', 'Not Paid') as Status\n" +
                    " \n" +
                    "FROM\n" +
                    "(\n" +
                    "SELECT LH.LoanNo, LH.RefNo, Led.LedgerName, LG.LedgerGroupName, Led.LedgerGroupID, LH.LoanAmount, \n" +
                    "Sum(Iif(LD.PaymentDueDate<=getdate(),LD.InstallmentAmt,0)) AS PendingTillDate,LH.BrokerName, LH.GroupName, \n" +
                    "VD1.TotalReceived, VD1.FStartDate,VD1.FEndDate,SUM(LD.InstallmentAmt) as TotalInstallAmt,\n" +
                    "LH.WithdrawMonth, LH.WithdrawAmt, LH.WithdrawDate, LH.BookingMonth, LH.BookingAmt, LH.BookingDate, LH.PaidMonth, LH.PaidAmt, LH.PaidDate\n" +
                    "FROM ((((LoanHeader AS LH INNER JOIN LoanDetail AS LD ON LH.LoanNo = LD.LoanNo) INNER JOIN Ledger AS Led \n" +
                    "ON LH.LedgerID = Led.LedgerID) INNER JOIN LedgerGroup AS LG ON Led.LedgerGroupID = LG.LedgerGroupID) LEFT JOIN \n" +
                    "(\n" +
                    "SELECT LD.LoanNo, IIF( Sum(VD.CreditAmt) > 0,Sum(VD.CreditAmt),0)  AS TotalReceived, \n" +
                    "Min(LD.PaymentDueDate) AS FStartDate, Max(LD.PaymentDueDate) As FEndDate\n" +
                    "FROM LoanHeader AS LH INNER JOIN (LoanDetail AS LD LEFT JOIN VoucherDetail AS VD ON LD.PosGUID = VD.RefID) ON LH.LoanNo = LD.LoanNo\n" +
                    "WHERE (LH.LoanRecType='G')  AND LH.Discontinued <> 1\n" +
                    "GROUP BY LD.LoanNo\n" +
                    ")  AS VD1  \n" +
                    "ON LH.LoanNo = VD1.LoanNo)\n" +
                    "WHERE (LH.LoanRecType='G')  AND LH.Discontinued <> 1\n" +
                    "GROUP BY LH.LoanNo, LH.RefNo, Led.LedgerName, LG.LedgerGroupName, Led.LedgerGroupID, LH.LoanAmount, \n" +
                    "LH.WithdrawMonth, LH.WithdrawAmt, LH.WithdrawDate, LH.BookingMonth, LH.BookingAmt, LH.BookingDate, LH.PaidMonth, LH.PaidAmt,\n" +
                    "LH.PaidDate,LH.BrokerName, LH.GroupName,VD1.TotalReceived,VD1.FStartDate,VD1.FEndDate\n" +
                    ") as L1\n" +
                    "WHERE  (TotalInstallAmt - TotalReceived) > 0";*/
            String sqlQry = fetchLoanQuery(connection);
            //endregion

            if(!Validation.isNullOrEmpty(sqlQry)){
                try {
                    PreparedStatement ps = connection.prepareStatement(sqlQry);
                    ResultSet rs = ps.executeQuery();

                    List<LoanBalance> loanBalances = new ArrayList<>();
                    LoanBalance lb;
                    while (rs.next()) {
                        lb = new LoanBalance();
                        lb.setLoanNo(rs.getInt(1));
                        lb.setFileNo(rs.getString(2));
                        lb.setLedgerName(rs.getString(3));
                        lb.setLedgerGroupName(rs.getString(4));
                        lb.setBrokerName(rs.getString(5));
                        lb.setGroupName(rs.getString(6));
                        lb.setLoanAmount(rs.getDouble(7));
                        lb.setPendingTillDate(rs.getDouble(8));
                        lb.setTotalReceived(rs.getDouble(9));
                        lb.setBalance(rs.getDouble(10));
                        lb.setDOF(rs.getString(11));
                        lb.setDOE(rs.getString(12));
                        lb.setPaidAmount(rs.getDouble(13));
                        lb.setStatus(rs.getString(14));
                        loanBalances.add(lb);
                    }

                    try {
                        realm.executeTransaction(t -> {
                            t.delete(LoanBalance.class);
                            t.insert(loanBalances);
                        });
                        pullAndSaved = true;
                    }
                    catch (Exception e) {
                        ErrorLog.LogError(e);
                    }
                }
                catch (Exception e) {
                    ErrorLog.LogError(e);
                }
            }
        }
        return pullAndSaved;
    }

    private static String fetchLoanQuery(Connection connection){
        String loanQry = "";
        String qryToFetchLoanQry = "select [SQL] From MsysReports Where ReportTitle = 'LoanBalanceSQL'";

        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(qryToFetchLoanQry);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                loanQry = rs.getString(1);
            }
        }
        catch (Exception e) {
            ErrorLog.LogError(e);
        }

        return loanQry;
    }

    public static List<LoanBalance> getAll(Realm realm){
        return realm.where(LoanBalance.class).findAll();
    }

    public static LoanBalance get(int loanNo, Realm realm) {
        return realm.where(LoanBalance.class).equalTo("LoanNo",loanNo).findFirst();
    }

    public static boolean deleteLoanRecordsInMobile(Realm realm) {
        boolean success = false;
        try {
            realm.executeTransaction(t->t.delete(LoanBalance.class));
            success = true;
        }
        catch (Exception e) {
            e.printStackTrace();
            ErrorLog.LogError(e);
        }
        return success;
    }

    //region Accessors
    public int getLoanNo() {
        return LoanNo;
    }
    public String getRefNo() {
        return RefNo;
    }
    public String getLedgerName() {
        return LedgerName==null? "Empty" : LedgerName;
    }
    public String getLedgerGroupName() {
       return LedgerGroupName==null? "Empty" : LedgerGroupName;
    }
    public double getPendingAmount() {
        return PendingAmount;
    }

    public String getFileNo() {
        return FileNo;
    }

    public String getBrokerName() {
        return BrokerName;
    }

    public String getGroupName() {
        return GroupName;
    }

    public double getLoanAmount() {
        return LoanAmount;
    }

    public double getPendingTillDate() {
        return PendingTillDate;
    }

    public double getTotalReceived() {
        return TotalReceived;
    }

    public double getBalance() {
        return Balance;
    }

    public double getPaidAmount() {
        return PaidAmount;
    }

    public double getTotalInstallAmount() {
        return TotalInstallAmount;
    }

    public String getStatus() {
        return Status;
    }

    public int getLedgerId() {
        return LedgerId;
    }

    public int getLedgerGroupId() {
        return LedgerGroupId;
    }

    public void setLoanNo(int loanNo) {
        LoanNo = loanNo;
    }

    public void setFileNo(String fileNo) {
        FileNo = fileNo;
    }

    public void setLedgerName(String ledgerName) {
        LedgerName = ledgerName;
    }

    public void setLedgerGroupName(String ledgerGroupName) {
        LedgerGroupName = ledgerGroupName;
    }

    public void setBrokerName(String brokerName) {
        BrokerName = brokerName;
    }

    public void setGroupName(String groupName) {
        GroupName = groupName;
    }

    public void setLoanAmount(double loanAmount) {
        LoanAmount = loanAmount;
    }

    public void setPendingTillDate(double pendingTillDate) {
        PendingTillDate = pendingTillDate;
    }

    public void setTotalReceived(double totalReceived) {
        TotalReceived = totalReceived;
    }

    public void setBalance(double balance) {
        Balance = balance;
    }

    public void setPaidAmount(double paidAmount) {
        PaidAmount = paidAmount;
    }

    public void setTotalInstallAmount(double totalInstallAmount) {
        TotalInstallAmount = totalInstallAmount;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public void setRefNo(String refNo) {
        RefNo = refNo;
    }

    public void setLedgerId(int ledgerId) {
        LedgerId = ledgerId;
    }

    public void setLedgerGroupId(int ledgerGroupId) {
        LedgerGroupId = ledgerGroupId;
    }

    public void setPendingAmount(double pendingAmount) {
        PendingAmount = pendingAmount;
    }

    public String getDOF() {
        return DOF;
    }

    public void setDOF(String DOF) {
        this.DOF = DOF;
    }

    public String getDOE() {
        return DOE;
    }

    public void setDOE(String DOE) {
        this.DOE = DOE;
    }

    //endregion

}
