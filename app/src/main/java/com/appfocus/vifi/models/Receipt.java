package com.appfocus.vifi.models;

import android.util.Log;

import com.appfocus.vifi.helpers.Formating;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.Sort;
import io.realm.annotations.PrimaryKey;

public class Receipt extends RealmObject {

    @PrimaryKey
    private String ReceiptGUID;
    private String ReceiptCode;
    private Date ReceiptDate;
    private double ReceiptAmt;
    private Date CreateTime;
    private Date UpdateTime;
    private int LoanNo;
    private String ReceiptComments;

    //region Methods

    public static String getNextReceiptCode(Realm realm){
        String receiptCode = null;

        CollectionAgent agent = realm.where(CollectionAgent.class).equalTo("IsLoggedIn",true).findFirst();
        if(agent!=null){
            int lastReceiptNo = agent.getLastReceiptNo();
            receiptCode = agent.getReceiptPrefix() + (lastReceiptNo + 1);
        }
        return receiptCode;
    }

    public static int getReceiptsCount(Realm realm) {
        return realm.where(Receipt.class).findAll().size();
    }

    public static Receipt get(String receiptGUID, Realm realm) {
        return realm.where(Receipt.class).equalTo("ReceiptGUID",receiptGUID).findFirst();
    }

    public static List<Receipt> getAll(Realm realm) {
        return realm.where(Receipt.class).sort("ReceiptCode", Sort.ASCENDING).findAll() ;
    }

    public static Boolean clearAll(Realm realm){
        boolean success = false;
        try {
            realm.executeTransaction(t->t.delete(Receipt.class));
            success = true;
        }
        catch (Exception e) {
            e.printStackTrace();
            ErrorLog.LogError(e);
        }
        return success;
    }

    public static Boolean AddOrUpdate(Receipt receipt, Realm realm){
        boolean success = false;

        try {
            realm.executeTransaction(t -> {

                if(Receipt.isNewReceipt(realm, receipt)) {

                    CollectionAgent agentLoggedIn =CollectionAgent.getLoggedInAgent(realm);

                    if (agentLoggedIn != null) {
                        int lastReceiptNo = agentLoggedIn.getLastReceiptNo();
                        agentLoggedIn.setLastReceiptNo(lastReceiptNo + 1);
                    }
                }
                t.copyToRealmOrUpdate(receipt);
            });
            success = true;
        }
        catch (Exception e) {
            e.printStackTrace();
            ErrorLog.LogError(e);
        }
        return success;
    }

    private static boolean isNewReceipt(Realm realm, Receipt receipt){
        boolean isNewReceipt = false;

        Receipt r = get(receipt.ReceiptGUID,realm);
        if(r==null)
            isNewReceipt = true;

        return isNewReceipt;
    }

    public static boolean push_update_clear(Connection connection, Realm realm) {
        boolean allDone = false;

        if(pushReceiptsToServer(connection,realm))
            if(updateLastReceiptNoInServer(connection,realm))
                if(clearReceiptsLocal(realm))
                    allDone = true;

        return allDone;
    }

    private static boolean pushReceiptsToServer(Connection connection,Realm realm) {
        boolean pushedReceipts=false;

        if(connection!=null){
            List<Receipt> receipts = Receipt.getAll(realm);
            CollectionAgent agent = CollectionAgent.getLoggedInAgent(realm);

            if(agent!=null){
                String strUserName  = agent.getUserName();
                String StrDeviceId = agent.getDeviceID();

                int noOfRowsInserted, totalNoOfRowsInserted = 0;

                try {
                    for (Receipt receipt:receipts) {
                        String strCode,strDate, strAmt, strGUID, strCreateTime,
                                strUpdateTime, strLoanNo, strComments;
                        strCode = receipt.ReceiptCode;
                        strDate = Formating.toString_SQL_Server(receipt.ReceiptDate);
                        strAmt = String.valueOf(receipt.ReceiptAmt);
                        strGUID = receipt.ReceiptGUID;
                        strCreateTime = Formating.toString_SQL_Server(receipt.CreateTime);
                        strUpdateTime = Formating.toString_SQL_Server(receipt.UpdateTime);
                        strLoanNo = String.valueOf(receipt.LoanNo);
                        strComments = receipt.ReceiptComments;

                        String sqlQry = String.format("insert into MobileReceipts (ReceiptGUID,ReceiptCode,ReceiptDate,ReceiptAmt, " +
                                        "CreateTime,UpdateTime,LoanNo,ReceiptComments,UserName,DeviceID) " +
                                        "values('%s','%s','%s',%s," +
                                        "'%s','%s',%s,'%s'," +
                                        "'%s','%s')",
                                strGUID, strCode,strDate,strAmt,
                                strCreateTime, strUpdateTime,strLoanNo,strComments,
                                strUserName,StrDeviceId);

                        PreparedStatement preparedStatement = connection.prepareStatement(sqlQry);
                        noOfRowsInserted = preparedStatement.executeUpdate();
                        totalNoOfRowsInserted+=noOfRowsInserted;
                    }
                    if(receipts.size()==totalNoOfRowsInserted)
                        pushedReceipts = true;
                }
                catch (Exception e) {
                    Log.d("ravikr", e.getMessage());
                    ErrorLog.LogError(e);
                }
            }
        }

        return pushedReceipts;
    }

    private static boolean updateLastReceiptNoInServer(Connection connection, Realm realm) {
        boolean updatedLastReceiptNo=false;

        if(connection!=null){
            CollectionAgent agentLoggedIn =  CollectionAgent.getLoggedInAgent(realm);

            if(agentLoggedIn!=null) {

                int lastReceiptNo = agentLoggedIn.getLastReceiptNo();
                int collectionAgentId = agentLoggedIn.getId();

                String sqlQry = String.format("update CollectionAgents " +
                        "set LastReceiptNo=%s " +
                        "where Id=%s",lastReceiptNo,collectionAgentId);

                try {
                    PreparedStatement preparedStatement = connection.prepareStatement(sqlQry);
                    int noOfRowsUpdated = preparedStatement.executeUpdate();
                    if(noOfRowsUpdated==1)
                        updatedLastReceiptNo=true;
                }
                catch (Exception e) {
                    Log.d("ravikr",e.getMessage());
                    ErrorLog.LogError(e);
                }
            }

        }
        return updatedLastReceiptNo;
    }

    private static boolean clearReceiptsLocal(Realm realm) {
        boolean clearedReceiptsLocal = false;

        try {
            realm.executeTransaction(t->t.delete(Receipt.class));
            clearedReceiptsLocal = true;
        }
        catch (Exception e) {
            Log.d("ravikr", e.getMessage());
            ErrorLog.LogError(e);
        }

        return  clearedReceiptsLocal;
    }

    //endregion

    //region Accessors
    public String getReceiptGUID() {
        return ReceiptGUID;
    }

    public void setReceiptGUID(String receiptGUID) {
        ReceiptGUID = receiptGUID;
    }

    public String getReceiptCode() {
        return ReceiptCode;
    }

    public void setReceiptCode(String receiptCode) {
        ReceiptCode = receiptCode;
    }

    public Date getReceiptDate() {
        return ReceiptDate;
    }

    public void setReceiptDate(Date receiptDate) {
        ReceiptDate = receiptDate;
    }

    public double getReceiptAmt() {
        return ReceiptAmt;
    }

    public void setReceiptAmt(double receiptAmt) {
        ReceiptAmt = receiptAmt;
    }

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }

    public Date getUpdateTime() {
        return UpdateTime;
    }

    public void setUpdateTime(Date updateTime) {
        UpdateTime = updateTime;
    }

    public int getLoanNo() {
        return LoanNo;
    }

    public void setLoanNo(int loanNo) {
        LoanNo = loanNo;
    }

    public String getReceiptComments() {
        return ReceiptComments;
    }

    public void setReceiptComments(String receiptComments) {
        ReceiptComments = receiptComments;
    }
    //endregion
}
