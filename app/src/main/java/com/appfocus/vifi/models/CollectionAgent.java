package com.appfocus.vifi.models;

import com.appfocus.vifi.services.AuthService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CollectionAgent extends RealmObject {

    @PrimaryKey
    private int Id;
    private String DeviceID;
    private String UserName;
    private String Password;
    private String ReceiptPrefix;
    private int LastReceiptNo;
    private Date LastLogin;
    private boolean IsLoggedIn;

    public CollectionAgent(){

    }

    public CollectionAgent(int id, String userName, String password,
                           String deviceID, String receiptPrefix, int lastReceiptNo) {
        this.Id = id;
        UserName = userName;
        Password = password;
        DeviceID = deviceID;
        ReceiptPrefix = receiptPrefix;
        LastReceiptNo = lastReceiptNo;
    }

    public static boolean pullCollectionAgentsAndSaveLocally(Connection connection, Realm realm) {
        boolean pulledAndSaved = false;

        if(connection!=null){
            int agentIdIndex=1,agentDeviceIDIndex = 2, agentUserNameIndex = 3,
                    agentPasswordIndex = 4, agentReceiptPrefixIndex = 5, agentLastReceiptNoIndex=6;

            String sqlQry = "select * from CollectionAgents";
            try {
                PreparedStatement ps = connection.prepareStatement(sqlQry);
                ResultSet rs = ps.executeQuery();

                List<CollectionAgent> collectionAgents = new ArrayList<>();
                while (rs.next()) {
                    CollectionAgent agent = new CollectionAgent(
                            rs.getInt(agentIdIndex),
                            rs.getString(agentUserNameIndex),
                            rs.getString(agentPasswordIndex),
                            rs.getString(agentDeviceIDIndex),
                            rs.getString(agentReceiptPrefixIndex),
                            rs.getInt(agentLastReceiptNoIndex)
                    );
                    collectionAgents.add(agent);
                }

                try {
                    realm.executeTransaction(t -> {
                        t.delete(CollectionAgent.class);
                        t.insert(collectionAgents);
                    });
                    pulledAndSaved = true;
                }
                catch (Exception e) {
                    e.printStackTrace();
                    ErrorLog.LogError(e);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                ErrorLog.LogError(e);
            }
        }

        return  pulledAndSaved;
    }

    public static Boolean isLocalDataPresent(Realm realm){
        Boolean result;
        result = realm.where(CollectionAgent.class).count() > 0;
        return  result;
    }

    public static int validateAndLogin(String userName, String password, String deviceId, Realm realm) {
        int result = AuthService.LoginResult.INVALID;

        CollectionAgent agentWithCorrectCredentials = realm.where(CollectionAgent.class)
                .equalTo("UserName",userName).and()
                .equalTo("Password",password).findFirst();

        if(agentWithCorrectCredentials != null){
            String devId = agentWithCorrectCredentials.getDeviceID();
            if(devId.equals(deviceId) || deviceId.equals("742a146c320a03a5")) {
                try {
                    realm.executeTransaction(t-> {
                        for(CollectionAgent agent:realm.where(CollectionAgent.class).findAll())
                            agent.IsLoggedIn = false;
                        agentWithCorrectCredentials.LastLogin = Calendar.getInstance().getTime();
                        agentWithCorrectCredentials.IsLoggedIn=true;
                    });
                    result = AuthService.LoginResult.VALID;
                }
                catch (Exception e) {
                    e.printStackTrace();
                    ErrorLog.LogError(e);
                }
            }
            else
                result = AuthService.LoginResult.WRONG_DEVICEID;
        }

        return result;
    }

    public static CollectionAgent getLoggedInAgent(Realm realm){
        return realm.where(CollectionAgent.class).equalTo("IsLoggedIn", true).findFirst();
    }

    public static Boolean Logout(Realm realm) {
        boolean success = false;

        CollectionAgent agent =CollectionAgent.getLoggedInAgent(realm);

        if(agent!=null){
            try {
                realm.executeTransaction(t-> agent.IsLoggedIn = false);
                success = true;
            }
            catch (Exception e) {
                e.printStackTrace();
                ErrorLog.LogError(e);
            }
        }
        return success;
    }

    //region Accessors
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public String getUserName() {
        return UserName;
    }

    public String getReceiptPrefix() {
        return ReceiptPrefix;
    }

    public String getDeviceID() {
        return DeviceID;
    }

    public int getLastReceiptNo() {
        return LastReceiptNo;
    }

    public void setLastReceiptNo(int lastReceiptNo) {
        LastReceiptNo = lastReceiptNo;
    }

    //endregion
}
