package com.appfocus.vifi.models;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class ApplicationSettings extends RealmObject {

    @Ignore
    final static String ID_FIXED = "ABC123";

    @PrimaryKey
    private String id;
    private String serverIPAddress;
    private String databaseName;
    private String databaseUserName;
    private String databasePassword;
    private boolean useBuiltInPrinter;

    public static void createInitialData(Realm realm, boolean areWeUnitTesting) {

        /*String serverIPAddress = "103.108.220.145";
        String databaseName = "ViFiDB";
        String databaseUserName = "vifi";
        String databasePassword = "uldK@879";*/

       /* String serverIPAddress = "192.168.1.102";
        String databaseName = "VIFISQL";
        String databaseUserName = "sa";
        String databasePassword = "ravi1234#";*/

        String serverIPAddress = "192.168.1.112";
        String databaseName = "VIFISQL";
        String databaseUserName = "MobileUsers";
        String databasePassword = "musers123#";
        boolean useBuiltInPrinter = true;

        ApplicationSettings applicationSettings = new ApplicationSettings
                (serverIPAddress,databaseName,databaseUserName,databasePassword,useBuiltInPrinter);

        if(areWeUnitTesting)
            realm.executeTransaction(t->t.copyToRealmOrUpdate(applicationSettings));
        else
            realm.copyToRealmOrUpdate(applicationSettings);
    }

    public ApplicationSettings(){
        this.id = ID_FIXED;
    }

    public ApplicationSettings(String serverIPAddress, String databaseName, String databaseUserName, String databasePassword, boolean useBuiltInPrinter) {

        this();
        this.serverIPAddress = serverIPAddress;
        this.databaseName = databaseName;
        this.databaseUserName = databaseUserName;
        this.databasePassword = databasePassword;
        this.useBuiltInPrinter = useBuiltInPrinter;
    }

    public static ApplicationSettings get(Realm realm){
        return realm.where(ApplicationSettings.class).findFirst();
    }

    public static boolean save(ApplicationSettings applicationSettings, Realm realm) {
        Boolean success = false;

        try {
            realm.executeTransaction(t -> t.copyToRealmOrUpdate(applicationSettings));
            success = true;
        }
        catch (Exception e) {
            e.printStackTrace();
            ErrorLog.LogError(e);
        }

        return  success;
    }

    //region Accessors

    public String getServerIPAddress() {
        return serverIPAddress;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public String getDatabaseUserName() {
        return databaseUserName;
    }

    public String getDatabasePassword() {
        return databasePassword;
    }

    public boolean getUseBuiltInPrinter() {
        return useBuiltInPrinter;
    }

    //endregion

}
