package com.appfocus.vifi.models;

import com.appfocus.vifi.helpers.Formating;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.Sort;
import io.realm.annotations.PrimaryKey;

public class ErrorLog extends RealmObject {

    @PrimaryKey
    private String Id;
    private Date LogDate;
    private String ErrorMessage;
    private String StackTrace;

    public ErrorLog(){}

    public ErrorLog(Exception ex) {
        Id = UUID.randomUUID().toString();
        LogDate = Calendar.getInstance().getTime();
        ErrorMessage = ex.getMessage();
        StackTrace = Arrays.toString(ex.getStackTrace());
    }

    public static void LogError(Exception ex){
        ErrorLog errorLog = new ErrorLog(ex);
        Realm realm = Realm.getDefaultInstance();
        try {

            realm.executeTransaction(t->{
                t.copyToRealmOrUpdate(errorLog);
            });
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<ErrorLog> GetAll(){
        List<ErrorLog> logs;
        Realm realm = Realm.getDefaultInstance();

        logs = realm.where(ErrorLog.class)
                .sort("LogDate", Sort.DESCENDING)
                .findAll();

        return logs!=null ? realm.copyFromRealm(logs):null;
    }

    public static ErrorLog Get(String Id){
        ErrorLog log;
        Realm realm = Realm.getDefaultInstance();

        log = realm.where(ErrorLog.class).equalTo("Id",Id).findFirst();

        return log!=null ? realm.copyFromRealm(log):null;
    }

    public static boolean ClearAll(){
        boolean success = false;
        Realm realm = Realm.getDefaultInstance();
        try {
            realm.executeTransaction(t->t.delete(ErrorLog.class));
            success = true;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

    //region Getters
    public String getId() {
        return Id;
    }

    public Date getLogDate() {
        return LogDate;
    }

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public String getStackTrace() {
        return StackTrace;
    }
    //endregion
}
