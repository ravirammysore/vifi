package com.appfocus.vifi.sqlconfig;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.StrictMode;
import android.util.Log;

import com.appfocus.vifi.models.ErrorLog;
import com.appfocus.vifi.models.ApplicationSettings;
import com.appfocus.vifi.services.SettingsService;

import java.sql.Connection;
import java.sql.DriverManager;

public class SQLServer {
    String serverIP;
    String databaseName;
    String databaseUserName;
    String databasePassword;

    private final Context ctx;

    SettingsService mSettingsService;

    public SQLServer(Context ctx) {
        super();
        this.ctx = ctx;

        mSettingsService = new SettingsService();
        ApplicationSettings applicationSettings = mSettingsService.getServerSettings();

        serverIP = applicationSettings.getServerIPAddress();
        databaseName = applicationSettings.getDatabaseName();
        databaseUserName = applicationSettings.getDatabaseUserName();
        databasePassword = applicationSettings.getDatabasePassword();
    }

    @SuppressLint("NewApi")
    public Connection GetConnection() {

        Connection connection = null;
        try {
            StrictMode.ThreadPolicy policy = new StrictMode
                    .ThreadPolicy
                    .Builder()
                    .permitAll()
                    .build();

            StrictMode.setThreadPolicy(policy);

            String connectionUrl = String.format("jdbc:jtds:sqlserver://%s;databaseName=%s;user=%s;password=%s;",
                    serverIP, databaseName, databaseUserName, databasePassword);

            Class.forName("net.sourceforge.jtds.jdbc.Driver"); //http://jtds.sourceforge.net/faq.html#noSuitableDriver
            connection = DriverManager.getConnection(connectionUrl);

        }
        catch (Exception ex) {
            Log.e("CON_ERROR", ex.getMessage());
            ErrorLog.LogError(ex);
        }

        return connection;
    }
}

