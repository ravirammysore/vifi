package com.appfocus.vifi.models;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import com.appfocus.vifi.services.AuthService;
import com.appfocus.vifi.services.LoanBalanceService;
import com.appfocus.vifi.services.ReceiptService;
import com.appfocus.vifi.services.SyncService;
import com.appfocus.vifi.services.TesterService;

import junit.framework.TestCase;

import org.junit.Assert;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class SmokeTest extends TestCase {

    private Context mContext = ApplicationProvider.getApplicationContext();

    TesterService testerService;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        testerService = new TesterService();
        testerService.deleteAllLocalRecords();

        testerService.createInitialAppSettings();

        Assert.assertTrue(testerService.arrangeDataInServer(mContext));

    }

    public void test_login_getLoans_createReceipts_pushReset_logoff() {

        //region Login
        AuthService authService = new AuthService(mContext);
        authService.Login("ravikr","1234","742a146c320a03a5");
        if(testerService.getNoOfAgentsLoggedIn()!=1)
            Assert.fail();
        CollectionAgent agent = authService.getLoggedInAgent();
        //endregion

        //region Pull loans
        SyncService syncService = new SyncService(mContext);
        syncService.pullLoanBalancesAndSaveLocally();
        LoanBalanceService loanBalanceService = new LoanBalanceService();
        List<LoanBalance> loans = loanBalanceService.getLoanBalances();
        LoanBalance loan1 = loans.get(0);
        LoanBalance loan2 = loans.get(1);
        //endregion

        //region Create receipts
        ReceiptService receiptService = new ReceiptService(mContext);
        Date todayDate = Calendar.getInstance().getTime();
        String receiptCode;

        receiptCode = receiptService.getNextReceiptCode();
        Receipt receipt1 = new Receipt(
                receiptCode,
                todayDate,
                UUID.randomUUID().toString(),
                5000.00,
                todayDate,
                todayDate,
                loan1.getLoanNo(),
                "Testing 1"
                );
        Assert.assertEquals("A1", receipt1.getReceiptCode());
        Assert.assertTrue(receiptService.AddOrUpdateReceipt(receipt1));

        receiptCode = receiptService.getNextReceiptCode();
        Receipt receipt2 = new Receipt(
                receiptCode,
                todayDate,
                UUID.randomUUID().toString(),
                8000.00,
                todayDate,
                todayDate,
                loan2.getLoanNo(),
                "Testing 2"
        );
        Assert.assertEquals("A2", receipt2.getReceiptCode());
        Assert.assertTrue(receiptService.AddOrUpdateReceipt(receipt2));

        Assert.assertEquals(2,receiptService.getReceiptsCount());
        //endregion

        //region Modify receipt
        Receipt receiptToModify = receiptService.getReceipt(receipt2.getReceiptGUID());
        Assert.assertEquals("A2", receiptToModify.getReceiptCode());

        receiptToModify.setReceiptAmt(15000.0);
        Assert.assertTrue(receiptService.AddOrUpdateReceipt(receiptToModify));
        Assert.assertEquals(2,receiptService.getReceiptsCount());
        Receipt modifiedReceipt = receiptService.getReceipt(receipt2.getReceiptGUID());
        Assert.assertEquals(15000.0, modifiedReceipt.getReceiptAmt(), 0.0);
        //endregion

        //region Push Receipts and clear local
        boolean allDone = receiptService.push_update_clear();
        Assert.assertTrue(allDone);

        Assert.assertEquals(0,receiptService.getReceiptsCount());
        //endregion

        //region Logout
        authService.Logout();
        Assert.assertNull(authService.getLoggedInAgent());
        //endregion
        
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}