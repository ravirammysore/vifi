package com.appfocus.vifi.models;

import junit.framework.TestCase;

import org.junit.Assert;

public class ErrorLogTest extends TestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ErrorLog.ClearAll();
    }

    public void testLogError() {
        Assert.assertEquals(0,ErrorLog.GetAll().size());

        for(int i= 1;i<=5;i++){
            try {
                Thread.sleep(1000);
                int res = divideOperation();
            }
            catch (Exception ex){
                ErrorLog.LogError(ex);
            }
        }

        Assert.assertEquals(5,ErrorLog.GetAll().size());

        try {
            Thread.sleep(1000);
            LoanBalance loanBalance=null;
            int ln = loanBalance.getLoanNo();
        }
        catch (Exception ex) {
            ErrorLog.LogError(ex);
        }
        Assert.assertEquals(6,ErrorLog.GetAll().size());

        ErrorLog.ClearAll();
        Assert.assertEquals(0,ErrorLog.GetAll().size());
    }

    private int divideOperation(){
       return 1/0;
    }
}