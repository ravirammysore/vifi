package com.appfocus.vifi.models;

import android.content.Context;
import androidx.test.core.app.ApplicationProvider;

import com.appfocus.vifi.services.SyncService;

import junit.framework.TestCase;

import org.junit.Assert;

public class LoanBalanceTest extends TestCase {

    private Context context = ApplicationProvider.getApplicationContext();

    public void testPullLedgersAndSaveLocally() {
        SyncService syncService = new SyncService(context);
        boolean success = syncService.pullLoanBalancesAndSaveLocally();
        Assert.assertTrue(success);
    }
}